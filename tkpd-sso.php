<?php
/**
 * @package TKPD-SSO
 */
/*
Plugin Name: TKPD-SSO
Plugin URI: https://bitbucket.org/topedbanana/seller/
Description: Used by Seller Center Website, Seller Core System is plugin to Activate SSO to this website
Version: 1.0
Author: Rio Bahtiar
Author URI: http://rio.my.id
License: Copyright to Tokopedia
Text Domain: tkpd
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

// tkpd-seller Plugin Directory map
define( 'TKPD_SSO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

// Destroy Data
// require_once( TKPD_SSO_PLUGIN_DIR . 'tkpd.unset-user-info.php' );

/**
* Function to Encrypt array or other data
*/


// Ugjgygj

// Encrtptor
function encode_tkpd($data) {
    return base64_encode(serialize($data));
}

function decode_tkpd($data) {
    return unserialize(base64_decode($data));
}

  // Get Data
require_once( TKPD_SSO_PLUGIN_DIR . 'tkpd-sso.connector.php' );